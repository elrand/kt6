

import org.junit.Test;

import java.util.*;
import java.util.ArrayList;

import static org.junit.Assert.assertTrue;


/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

    /**
     * Main method.
     */
    public static void main(String[] args) {
        GraphTask a = new GraphTask();
        a.run();
    }

    /**
     * Finding the path between start vertex to destination vertex through the highest vertex.
     * Actual main method to run examples and everything.
     */
    public void run() {
        //insert the nr of vertexes
        int vertices = 6;

        //insert the nr of edges
        int edges = 6;

        //insert the starting point
        int start = 3;

        //insert the destination
        int destination = 6;

        Graph g = new Graph("G");

        // TODO!!! Your experiments here

        //creating adjacency list
        ArrayList<ArrayList<Integer>> adjList = g.createRandomSimpleGraph(vertices, edges);

        //printing out the graph and path
        System.out.println(g);
        System.out.println("start: " + start + "  destination: " + destination);
        Graph.printPath(adjList, start - 1, destination - 1, vertices);




    }


    static class Vertex {

        private String id;
        private Vertex next;
        private Arc first;
        private int info = 0;
        private int height = getRandomInteger();


        /**
         * Method to get a random int between the highest and the lowest point on Earth.
         * @return random int
         */
        private int getRandomInteger() {
            return (int) (Math.random() * ((8848 + 413) + 1)) - 413;
        }

        Vertex(String s, Vertex v, Arc e) {
            id = s;
            next = v;
            first = e;
        }

        Vertex(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }
    }


    /**
     * Arc represents one arrow in the graph. Two-directional edges are
     * represented by two Arc objects (for both directions).
     */
    static class Arc {

        private String id;
        private Vertex target;
        private Arc next;

        Arc(String s, Vertex v, Arc a) {
            id = s;
            target = v;
            next = a;
        }

        Arc(String s) {
            this(s, null, null);
        }

        @Override
        public String toString() {
            return id;
        }

    }


    public static class Graph {


        private String id;
        private static Vertex first;
        int highestHeight = -414;
        private static Vertex highestVertex;

        Graph(String s, Vertex v) {
            id = s;
            first = v;
        }

        Graph(String s) {
            this(s, null);

        }


        @Override
        public String toString() {


            String nl = System.getProperty("line.separator");
            StringBuilder sb = new StringBuilder(nl);
            sb.append(id);
            sb.append(nl);
            Vertex v = first;
            while (v != null) {

                sb.append(v.toString());
                sb.append(" h=").append(v.height);
                if (v.height > highestHeight) {
                    highestHeight = v.height;
                    highestVertex = v;
                }
                sb.append(" -->");
                Arc a = v.first;
                while (a != null) {
                    sb.append(" ");
                    sb.append(a.toString());
                    sb.append(" (");
                    sb.append(v.toString());
                    sb.append("->");
                    sb.append(a.target.toString());
                    sb.append(")");
                    a = a.next;
                }
                sb.append(nl);
                v = v.next;
            }
            sb.append("Highest vertex is: ").append(highestVertex);
            return sb.toString();
        }

        public Vertex createVertex(String vid) {
            Vertex res = new Vertex(vid);
            res.next = first;
            first = res;
            return res;
        }

        public void createArc(String aid, Vertex from, Vertex to) {
            Arc res = new Arc(aid);
            res.next = from.first;
            from.first = res;
            res.target = to;
        }

        /**
         * Create a connected undirected random tree with n vertices.
         * Each new vertex is connected to some random existing vertex.
         *
         * @param n number of vertices added to this graph
         */
        public void createRandomTree(int n) {
            if (n <= 0)
                return;
            Vertex[] varray = new Vertex[n];
            for (int i = 0; i < n; i++) {
                varray[i] = createVertex("v" + (n - i));
                if (i > 0) {
                    int vnr = (int) (Math.random() * i);
                    createArc("a" + varray[vnr].toString() + "_"
                            + varray[i].toString(), varray[vnr], varray[i]);
                    createArc("a" + varray[i].toString() + "_"
                            + varray[vnr].toString(), varray[i], varray[vnr]);
                }
            }
        }

        /**
         * Create an adjacency matrix of this graph.
         * Side effect: corrupts info fields in the graph
         *
         * @return adjacency matrix
         */
        public static int[][] createAdjMatrix() {
            int info = 0;
            Vertex v = first;
            while (v != null) {
                v.info = info++;
                v = v.next;
            }
            int[][] res = new int[info][info];
            v = first;
            while (v != null) {
                int i = v.info;
                Arc a = v.first;
                while (a != null) {
                    int j = a.target.info;
                    res[i][j]++;
                    a = a.next;
                }
                v = v.next;
            }
            return res;
        }

        /**
         * Create a connected simple (undirected, no loops, no multiple
         * arcs) random graph with n vertices and m edges.
         *
         * @param n number of vertices
         * @param m number of edges
         * @return return
         */
        public ArrayList<ArrayList<Integer>> createRandomSimpleGraph(int n, int m) {
            if (n <= 0)
                return null;
            if (n > 2500)
                throw new IllegalArgumentException("Too many vertices: " + n);
            if (m < n - 1 || m > n * (n - 1) / 2)
                throw new IllegalArgumentException
                        ("Impossible number of edges: " + m);
            first = null;
            createRandomTree(n);       // n-1 edges created here
            Vertex[] vert = new Vertex[n];
            Vertex v = first;
            int c = 0;
            while (v != null) {
                vert[c++] = v;
                v = v.next;
            }
            int[][] connected = createAdjMatrix();
            int edgeCount = m - n + 1;  // remaining edges
            while (edgeCount > 0) {
                int i = (int) (Math.random() * n);  // random source
                int j = (int) (Math.random() * n);  // random target
                if (i == j)
                    continue;  // no loops
                if (connected[i][j] != 0 || connected[j][i] != 0)
                    continue;  // no multiple edges
                Vertex vi = vert[i];
                Vertex vj = vert[j];
                createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
                connected[i][j] = 1;
                createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
                connected[j][i] = 1;
                edgeCount--;  // a new edge happily created
            }
            return Graph.toArraylists(connected);
        }


        //-----------------------------------------------------------------------

        /**
         * Method to print out the path from start to destination that goes through the highest vertex.
         * Calls out breathFirstSearch method.
         * If the start or the destination is not the highest vertex breathFirstSearch is called twice.
         * @param adjacencyList list of connected vertices
         * @param start satrting point
         * @param destination ending point
         * @param vertices number of vertices
         */
        public static void printPath(ArrayList<ArrayList<Integer>> adjacencyList, int start, int destination, int vertices) {

            LinkedList<Arc> path = new LinkedList<>();
            int[] predecessorList = new int[vertices];
            int[] distance = new int[vertices];
            int highestVertexnr = Integer.parseInt(highestVertex.id.replaceAll("\\D+", "")) -1 ;
            if (vertices<=start){
                throw new RuntimeException("Starting point should not be bigger than the number of vertices!!");
            }if (vertices<=destination){
                throw new RuntimeException("Destination should not be bigger than the number of vertices!!");

            }


            if (start == (highestVertexnr)) {
                if (!(breadthFirstSearch(adjacencyList, start, destination, vertices, predecessorList, distance))) {
                    System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
                    return;
                }
                int current = destination;
                path.add(new Arc(Integer.toString(current)));
                while (predecessorList[current] != -1) {
                    path.add(new Arc(Integer.toString(predecessorList[current])));
                    current = predecessorList[current];
                }

            } else if (destination == (highestVertexnr)) {
                if (!(breadthFirstSearch(adjacencyList, start, highestVertexnr, vertices, predecessorList, distance))) {
                    System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
                    return;
                }
                int current = highestVertexnr;
                path.add(new Arc(Integer.toString(current)));
                while (predecessorList[current] != -1) {
                    path.add(new Arc(Integer.toString(predecessorList[current])));
                    current = predecessorList[current];
                }

            } else {
                if (!breadthFirstSearch(adjacencyList, highestVertexnr, destination, vertices, predecessorList, distance)) {
                    System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
                    return;
                }
                int current = destination;
                path.add(new Arc(Integer.toString(current)));
                while (predecessorList[current] != highestVertexnr) {
                    path.add(new Arc(Integer.toString(predecessorList[current])));
                    current = predecessorList[current];
                }


                if (!breadthFirstSearch(adjacencyList, start, highestVertexnr, vertices, predecessorList, distance)) {
                    System.out.println("There is no path from " + start + " to " + destination + " that goes through the highest vertex " + highestVertex);
                    return;
                }
                int current2 = highestVertexnr;
                path.add(new Arc(Integer.toString(current2)));
                while (predecessorList[current2] != -1) {
                    path.add(new Arc(Integer.toString(predecessorList[current2])));
                    current2 = predecessorList[current2];
                }
            }

            System.out.println("Path is :");
            for (int i = path.size() - 1; i >= 0; i--) {
                int ans = Integer.parseInt(path.get(i).id) + 1;
                System.out.print(ans);
                if (i > 0) {
                    System.out.print(" -> ");
                }

            }
        }

        /**
         * breadthFirstSearch method to go through the graph and find the path from start to destination.
         * @param adjacencyList list of connected vertices
         * @param start starting point
         * @param destination destination
         * @param vertices number of vertices
         * @param pred list of ints that contains the info about predecessors
         * @param dist list of distances from starting point
         * @return boolean if the vertices are connected
         */
        private static boolean breadthFirstSearch(ArrayList<ArrayList<Integer>> adjacencyList,
                                                  int start, int destination, int vertices, int[] pred, int[] dist){
            LinkedList<Integer> queue = new LinkedList<>();
            boolean[] visited = new boolean[vertices];

            for (int i = 0; i < vertices; i++) {
                visited[i] = false;
                dist[i] = Integer.MAX_VALUE;
                pred[i] = -1;
            }

            visited[start] = true;
            dist[start] = 0;
            queue.add(start);

            while (!queue.isEmpty()) {
                int j = queue.remove();
                for (int i = 0; i < adjacencyList.get(j).size(); i++) {
                    int current = adjacencyList.get(j).get(i);
                    if (!visited[current]) {
                        visited[current] = true;
                        dist[current] = dist[j] + 1;
                        pred[current] = j;
                        queue.add(current);

                        if (current == destination) {
                            return true;
                        }
                    }
                }
            }
            return false;

        }


        /**
         * method to convert adjacency matrix to adjacency list, that it would be easily readable.
         * @param adj adjacency matrix
         * @return adjacency list
         */
        public static ArrayList<ArrayList<Integer>> toArraylists(int[][] adj) {
            ArrayList<ArrayList<Integer>> lists = new ArrayList<>();
            for (int[] ints : adj) {
                ArrayList<Integer> list = new ArrayList<>();
                for (int i = 0; i < ints.length; i++) {
                    if (ints[i] == 1) {
                        list.add(i);
                    }
                }
                lists.add(list);
            }
            return lists;
        }
    }
}